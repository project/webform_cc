Adds CC fields to webform emails.  No guarantees or warrantees.
The patch file merely affects formatting on email edit page. 
It is not necessary for use but it might be difficult to make
edits without it.